# Stears Coding Homework

Thanks for your application to Stears

This is a coding homework test for Stears’ Election Center Technical Team Lead. We expect that you will use this test to demonstrate your abilities. Your solution does not need to be perfect, but keep in mind we are hiring for someone to build a scalable real-time system so you must know exactly what needs to be done to take the solution to the next level.

For this role, we prefer fullstack software engineer candidates with:
- Experience building backend with Nodejs
- Experience building frontend with Reactjs

For each task, you will be given a back story and a couple of tasks to summarise what needs to be done with a strict definition of done. Either complete all of them or attempt as much as possible so we have enough information to assess your skill & experience.

#### Submissions

- Create a private [gitlab](https://gitlab.com/) repository and add @foluso_ogunlana & @ssani as maintainers
- Create a README.md with the following:
  - Clear set up instructions (assume no knowledge of the submission or the stack)
  - Notes of any new useful tools / patterns you discovered while completing the test

#### Deadlines

You will be given at least 5 working days to complete the task and your deadline will be communicated via email.

#### Assessment

Our favourite submissions are -

1. **Clean & simple code** - Minimal, high quality and well tested code that gets to the heart of the problem and stops there.
2. **Rigorous** - Complete solutions with well researched applications of the right technology choices.
3. **UX & DevX** - Smooth user experience which is easy to deploy, well documented and easy to contribute to.

Your submission will also impress us by demonstrating one or more of the following -

- **Mastery** in your general use of language, libraries, frameworks, architecture / design / deployment patterns
- **Unique skills** in specific areas - particularly data engineering, devops & testing

#### Conduct

By submitting, you assure us that you have not shared the test with anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources you see fit to learn skills for the tasks.

# **Coding Homework**

**Guidelines**
- Complete all tasks if possible
- Include a README.md telling us how to use your code
- FE - Use React or [NextJS](https://nextjs.org/docs) and use modern React patterns, e.g. Hooks, functional components
- BE - Using a framework like Express, NestJS or NextJS will save you time. If you don't, provide adequate documentation so we don't lose time figuring out how to run the code.

## Backend

### Background

You work for the NNC (Nigerian News Company) as a backend developer and will be supporting the weather channel. You’ve been asked to build a Weather API for the front-end developers to consume. You’ll be consuming data provided daily by the team meteorologist, Prof. Mua. 

Every morning, Prof. Mua provides you with a file containing a list of geographical regions, the Celcius temperature of the region, the probability of precipitation at the moment, and the exact timestamp indicating when these readings were taken. The front-end engineers need you to design an API to allow them to fetch all the temperatures and rain information for a list of regions to be displayed on a map. Can you write an API to serve their needs?

Note - You can assume that the individual geographical regions are states within a larger region (e.g. a country)

### Task 

Load a SQL database with the weather data provided file (e.g. CSV)  so that it can power your API in the next task. The database should store the region, temperature, probability of rain, and timestamp for each data point. Your means of loading the data should be easy and repeatable as you will hand it over to Prof. Mua to load her entries without your supervision.

Hints:

- You can use the example [ng.csv](/ng.csv), or you can make your own file in CSV format or other
  - You can assume that Prof. Mua’s file is in a structured format, e.g. CSV.
  - You are allowed to add more columns to the CSV if needed for further categorisation
  - Less is more - If you find an easy and repeatable way to load the database, use it, even if it means you have to ditch the CSV and use another data format. We only want to test that you can store and update data in a database in a simple repeatable manner, not your ability to parse CSVs. Though parsing it shouldn't be much trouble.

**Definition of done**:

- [ ] Must have a means of loading the data into database rows (a CLI command is fine, but an external tool or library or direct database connection works too)
- [ ] Must have a means of easily updating the existing data in the database (e.g. loading new rows from a CSV)
- [ ] Must successfully load all rows from the file into the database on a single and repeatable command or action
- [ ] The database must store the latest information for each region, judging by the region’s timestamp (for instance, if there are duplicate readings for the same region)
- [ ] Loading the CSV must fail with a helpful message if any row contains missing information
- [ ] Loading an empty CSV must not fail, but should have no effect
- [ ] Loading the same CSV multiple times must not fail, but should have no effect

## Frontend

### Background

You’ve created an API that returns the weather data for each state in the list. Unfortunately the front-end engineers are swamped with another NNC project. Since you are a full stack engineer, you volunteer to create the barebones UI and the frontend engineers can pick it up when they return.

The team wants to build a ReactJS based weather map of Nigeria, that shows the temperature in each state, and whether or not it’s raining. The NNC will use this map to broadcast the daily weather forecast on their website. You just need to create the map and show the information on it.

### Task

Create a map UI to fetch and visualise the weather data. Use your favourite GeoJSON compatible map visualisation library. The map should clearly show each state in Nigeria and should be colour coded based on temperature (Feel free to select 5-10 hex codes to use). On hover, display the name of the region, temperature figure, the probability of rain and timestamp

Hints:

- You can use the example [ng.json](/ng.json), or create your own map file with the following instructions
  - Use a high level library to render the map. Favour GeoJSON!
  - Get a map here: ​​https://gadm.org/download_country.html
  - Test or convert to your desired format here: https://mapshaper.org/
  - Simplify the GeoJSON until it’s less than 100KiB, and you only need the states!
  - For GeoJSONs, you might also need to “rewind” the GeoJSON if it does not correctly display the features you expect

**Definition of done**:

- [ ] Must display a map of Nigeria showing all states in the country
- [ ] On hover, each state must display the data fetched from the API in a tooltip / popup
- [ ] Must colour-code the the different states on the map based on the temperature (at least 3 different colours or shades of the same colour)
- [ ] If the database is updated and the page is reloaded, the map must show the new information fetched via the API

## Full Stack

### Background

Your map is a hit! Prof. Mua is ecstatic, but she needs one more thing…

The NNC has asked her to provide hourly updates to the weather forecast based on her latest readings so that the newscasters can start the weather show any time of the day. Fortunately, your API is always up to date, as Prof. Mua only needs to continuously re-enter the data with the method you created in Task 1. However, the newscasters’ weather browser is not able to “reload” itself to show the latest data. You’ll need to “push” the new data to the browser. 

### Task

Make your map real-time so that any change on the database is immediately reflected on the customer’s browser without a page refresh

### Definition of done:

- [ ] The map must show the latest information for all states without page refreshes
- [ ] On hover, each state must display the latest information without a page refresh, and without the popup going out of focus
- [ ] A user must be able to see clearly that something has changed and exactly what the change is and when it changed

## Thanks!

If you made it this far, thanks for your time.
We look forward to reviewing your solid application with you!
